# README #

Here you can find some aux. python scripts for JMeter on Pramerica

### Requirements ###

* Python 3.6+
#### Packages ####
* lxml
* termcolor.colored
* termcolor
* yaml
* Deployment instructions

### jdbcGen.py ###

* jdbcGen [-l --sqllog] <sql log to parse> [-o --output] <output JMX file>  [-i --input] <input JMX file>

### schemeAppend.py ###

* schemeAppend [-y --yaml] <yaml with substrings> [-i --input] <input JMX file>  [-o --output] <output JMX file>